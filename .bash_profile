#save in home folder
#gets rid of any XOFF onscreen messages when pressing Ctrl-S.
#Note, after you make changes to your .bash_profile you have to re-run it with the command 'source .bash_profile' or logout/login for  changes to take effect.
#for more info http://stackoverflow.com/questions/3446320/in-vim-how-to-map-save-to-ctrl-s
#Delete comments after saving
bind -r '\C-s'
stty -ixon
